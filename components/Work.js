import React, { Profiler } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Image from "next/image";
import img from "../public/imgs/bg.jpg"
import { useRouter } from "next/router";

export const Skills = ({ title, cards, icons }) => {
  return (
    <div id="skills" className="py-5 px-5">
      <div className="container">
        <h1 className="text-center" style={{color: "#212529"}}>{title}</h1>
        <div className="d-flex flex-row flex-wrap justify-content-center" style={{padding:0}}>
          {cards.map((value, index) => (
            <Card1
              key={index}
              title={value.title}
              description={value.description}
              link={value.link}
              icons={value.icons}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export const Projects = ({ title, cards }) => {
  return (
    <div id="destinations" className="py-5 px-5">
      <div className="container">
        <h1 className="text-center" style={{color:"#112b3c"}}>Top Cities & Destinations</h1>
        <div className="d-flex flex-row flex-wrap justify-content-center">
          {cards.map((value, index) => (
            <Card
              key={index}
              title={value.title}
              description={value.description}
              icons={value.icons}
            />
          ))}
        </div>
        {/* <div className="text-center">
					<button type="button" className="btn btn-outline-light">See More</button>
				</div> */}
      </div>
    </div>
  );
};

export const Card = ({ title, description }) => {
  return (
    <div
      className="card py-3 px-3 mx-sm-4 my-4 card-work"
      style={{ width: "20rem", height:"200px" }}
    >
      <Image src={img} layout="fill" />
      <h4 className="text-white text-center mt-5" style={{zIndex:"200", fontStyle:"Outfit", fontWeight:"500", letterSpacing:1.5, fontSize:32}}>{title}</h4>
      {/* <h4 className="text-primary">{title}</h4> */}
      <div className="text-dark text-center">{description}</div>
      {/* <div className="text-end">
        <div>&rarr;</div>
      </div> */}
    </div>
  );
};

export const Card1 = ({ title, description, link }) => {

  const router = useRouter()
  return (
    <div
      className="card mx-sm-4 my-4 card-work"
      style={{ width: "20rem", borderRadius:5, cursor:"pointer",}}
      onClick={()=>router.push("/package-details")}
    >
      <Image src={img} width={200} height={200} />
      <h4 className="text-center" style={{color: "#112B3C", fontStyle:"Lato"}}>{title}</h4>
      {/* <h4 className="text-primary">{title}</h4> */}
      <div className="text-dark text-center">{description}</div>
      {/* <div className="text-end">
        <div>&rarr;</div>
      </div> */}
    </div>
  );
};
