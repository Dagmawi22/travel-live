import React, { Profiler } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Image from "next/image";
import img from "../public/imgs/bg.jpg"

export const Exclusive = ({ title, cards, icons }) => {
  return (
    <div id="skills" className="py-4">
      <div className="container">
        <h1 className="text-center" style={{color: "#212529"}}>{title}</h1>
        <div className="d-flex flex-row flex-wrap justify-content-around" style={{}}>
          {cards.map((value, index) => (
            <Card
              key={index}
              title={value.title}
              description={value.description}
              link={value.link}
              icons={value.icons}
            />
          ))}
        </div>
      </div>
    </div>
  );
};


export const Card = ({ title, description }) => {
  return (
    <div class="d-flex justify-content-center" style={{marginRight:20, marginTop:20}}>

        <div class="card" style={{width: "35rem", maxHeight:200}}>
        <div class="d-flex justify-content-between">
            <div className="px-2 py-2">
                <Image src={img} width={134} height={134} />
            </div>
            <div class="card-body">
    <h5 class="card-title">Get Up to 25% OFF* on Flights, Hotels & Holidays</h5>
    <p class="card-text">& get your next refreshing break sorted!.</p>
  </div>
        </div>
  
</div>
    </div>
  );
};

export const Card1 = ({ title, description }) => {
  return (
    <div
      className="card mx-sm-4 my-4 card-work"
      style={{ width: "20rem", borderRadius:5 }}
    >
      <Image src={img} width={200} height={200} />
      <h4 className="text-center" style={{color: "#112B3C", fontStyle:"Lato"}}>{title}</h4>
      {/* <h4 className="text-primary">{title}</h4> */}
      {/* <div className="text-dark text-center">{description}</div> */}
      {/* <div className="text-end">
        <div>&rarr;</div>
      </div> */}
    </div>
  );
};
