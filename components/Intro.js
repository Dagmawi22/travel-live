import React from 'react';
import getConfig from 'next/config'
import Link from 'next/link'
import { useEffect, useState } from 'react';
import bg from "../public/imgs/bg.jpg"
import bg2 from "../public/imgs/bg2.jpg"
import bg3 from "../public/imgs/bg3.jpg"
import Image from 'next/image';
import { faPersonWalking, faLocationDot, faCircleDot } from '@fortawesome/free-solid-svg-icons';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useRouter } from 'next/router';

const { publicRuntimeConfig } = getConfig()

export const Intro = ({ title, description, image, buttons }) => {

	const [img, setImg] = useState(1)

	const [windowWidth, setWindowWidth] = useState(0)

	const router = useRouter()

	useEffect(() => {
		setWindowWidth(window.innerWidth)
		setTimeout(() => {
		  if(img>2){
			setImg(1)
		  }else{
			setImg((img) => img + 1);
		  }
			
		}, 4000);
	  });

	return (
		<div className="" style={{width:"100vw"}}>
			<div className="">
				<div className="row align-items-center">
					
					<div className="col-sm-12 text-center">
						{
							img==1 && 
							<Image
							className="my-3" width={windowWidth}
							height="550" src={bg}
							alt="profile of travel"
						/>
						}

{
							img==2 && 
							<Image
							className="my-3" width={windowWidth}
							height="550" src={bg2}
							alt="profile of travel"
						/>
						}

{
							img==3 && 
							<Image
							className="my-3" width={windowWidth}
							height="550" src={bg3}
							alt="profile of travel"
						/>
						}
						
						
						
					</div>

					<div style={{display:"flex", flexDirection:"row",
						marginTop:"-150px", color:"#fff", zIndex:"300",
						alignItems:"center", justifyContent:"center"
					}}>
						{
							img == 1 && 
							<>
						<span style={{fontSize:95, marginTop:"-12px"}}>&#8226;</span>
						<span style={{fontSize:50}}>&#9675;</span>
						<span style={{fontSize:50}}>&#9675;</span>
						</>
						}

{
							img == 2 && 
							<>
							
						<span style={{fontSize:50}}>&#9675;</span>
						<span style={{fontSize:95, marginTop:"-12px"}}>&#8226;</span>
						<span style={{fontSize:50}}>&#9675;</span>
						</>
						}

{
							img == 3 && 
							<>
							
						<span style={{fontSize:50}}>&#9675;</span>
						<span style={{fontSize:50}}>&#9675;</span>
						<span style={{fontSize:95, marginTop:"-12px"}}>&#8226;</span>
						</>
						}

						
						
						</div>

					<div className="col-sm-12 text-center" style={{marginTop:"-600px", zIndex:100}}>
						<h3 className="" style={{
							color:"white",
							letterSpacing:1.6,
							fontSize:35,
							fontWeight:500,
						}}>{title}</h3>

						<div style={{padding:10, marginTop:20, backgroundColor:"#e6e6f2", height:220, width:"70vw", marginLeft:"15vw", borderRadius:10,
						boxShadow: "5px 5px #888888"
					}}>
							<h3 className="" style={{
							letterSpacing:1.6,
							fontSize:20,
							fontWeight:600,
							marginTop:20
						}}>
							FIND EXPERIENCES AND ACTIVITIES!
					</h3>


					<div style={{marginTop:20, backgroundColor:"#fff", height:100, width:"90%", marginLeft:"5%", borderRadius:10,
						
					}}>
						<div style={{display:"flex", flexDirection:"row"}}>
							<div style={{marginLeft:"2%", width:"32%"}}>
								<FontAwesomeIcon icon={faPersonWalking} style={{color:"red", fontSize: 30, marginRight:15, marginTop:10}}/>
							<a>Activity</a>
							<div>
							<select style={{marginTop:"10px", height:50, width:"100%", border:"none", backgroundColor:"white"}}>
								<option>a</option>
								<option>b</option>
								<option>c</option>
							</select>
							</div>
							</div>

							<div style={{marginLeft:"8%", width:"33%"}}>
							<FontAwesomeIcon icon={faLocationDot} style={{color:"red", fontSize: 30, marginRight:15, marginTop:10}}/>
							<a>Location</a>

							<div>
							<select style={{marginTop:"10px", height:50, width:"100%", border:"none", backgroundColor:"white"}}>
								<option>a</option>
								<option>b</option>
								<option>c</option>
							</select>
							</div>
							</div>

							<div style={{
								marginLeft:"0%", width:"25%", height:100, backgroundColor:"#2152B1",
								alignItems:"center", justifyContent:"center"
								}}
								className='text-center'
								onClick = {()=>router.push("/search-results")} 
								>
								<p style={{
									color:"#fff",
									fontSize:20,
									marginTop:35,
									fontWeight:700,
									cursor:"pointer"
								}}>SEARCH</p>
							</div>

							</div>
						</div>

						</div>
						
						</div>
					

				</div>
			</div>
		</div>
	);
}

export const About = ({ title, description }) => {
	return (
		<div id="about" className="bg-white py-5 px-5">
			<div className="container">
				<h1 className="text-primary fw-bold">{title}</h1>
				<div className="px-sm-5">
					{description.map((value, index) => (
						<p key={index} >{value}</p>
					))}
				</div>
			</div>
		</div>
	);
}