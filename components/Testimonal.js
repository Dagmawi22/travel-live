import React, { Profiler } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faQuoteRight } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import Image from "next/image";
import img from "../public/imgs/bg.jpg"


export const Testimonal = ({ title, cards }) => {
  return (
    <div id="testimonials" className="py-5 px-5">
      <div className="container">
        <p className="text-center" style={{color: "#ED5826", fontSize:16}}>TESTIMONIAL</p>
        <p className="text-center" style={{color: "#112b3c", fontSize:48, fontWeight:700}}>
        Satisfied <span style={{color: "#ED5826"}}>travellers</span> around the world
        </p>
        <div className="d-flex flex-row flex-wrap justify-content-center" style={{padding:0}}>
          {cards.map((value, index) => (
            <Card1
              key={index}
              title={value.title}
              description={value.description}
              link={value.link}
              icons={value.icons}
              name={value.name}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export const Projects = ({ title, cards }) => {
  return (
    <div id="projects" className="py-5 px-5">
      <div className="container">
        <h1 className="text-center" style={{color:"#112b3c"}}>TESTIMONIAL</h1>
        <div className="d-flex flex-row flex-wrap justify-content-around">
          {cards.map((value, index) => (
            <Card
              key={index}
              title={value.title}
              description={value.description}
              icons={value.icons}
            />
          ))}
        </div>
        {/* <div className="text-center">
					<button type="button" className="btn btn-outline-light">See More</button>
				</div> */}
      </div>
    </div>
  );
};

export const Card = ({ title, description }) => {
  return (
    <div
      className="card py-3 px-3 mx-sm-4 my-4 card-work"
      style={{ width: "20rem", height:"200px" }}
    >
      <Image src={img} layout="fill" />
      <h4 className="text-white text-center mt-5" style={{zIndex:"200", fontStyle:"Outfit", fontWeight:"500", letterSpacing:1.5, fontSize:32}}>{title}</h4>
      {/* <h4 className="text-primary">{title}</h4> */}
      <div className="text-dark text-center">{description}</div>
      {/* <div className="text-end">
        <div>&rarr;</div>
      </div> */}
    </div>
  );
};

export const Card1 = ({ name, title, description }) => {
  return (
    <div style={{width:"22rem", backgroundColor:"#fafafa", marginRight:20, marginTop:20}} className="py-3 px-5">
        
        <div style={{marginTop:-30, marginLeft:"100%", backgroundColor:"#ed5826", color:"#fff", padding:"10px 20px", borderRadius:"50%"}}>
        <FontAwesomeIcon icon={faQuoteRight} style={{marginLeft:-7}}/>
        </div>

        <div style={{marginLeft:"40%"}}><Image src={img} width={70} height={70} style={{borderRadius:50}} /></div>
        <div className="text-center" style={{color: "#112b3c", fontSize:25, fontWeight:700}}>
        {name}
        </div>

        <div className="text-center" style={{color: "#112b3c", fontSize:20, fontWeight:300}}>
        {title}
        </div>

        <div className="text-center" style={{color: "#ffbd39", fontSize:20, fontWeight:300}}>
            <FontAwesomeIcon icon={faStar} />
            <FontAwesomeIcon icon={faStar} />
            <FontAwesomeIcon icon={faStar} />
            <FontAwesomeIcon icon={faStar} />
            <FontAwesomeIcon icon={faStar} />
        </div>

        <div className="mt-3" style={{textAlign:"justify", color: "#112b3c", fontSize:20, fontWeight:300}}>
        {description}
        </div>

    </div>
  );
};
