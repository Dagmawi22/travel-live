import React, { useEffect, useState } from 'react';
import Link from 'next/link'

import { faUserPlus } from '@fortawesome/free-solid-svg-icons';
import { faRightToBracket } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping, faCircleXmark } from '@fortawesome/free-solid-svg-icons';
import logo from "../public/logo.svg"
import Image from 'next/image';

export const Nav = ({ title, links }) => {

  const [isNavCollapsed, setIsNavCollapsed] = useState(true);

  const handleNavCollapse = () => setIsNavCollapsed(!isNavCollapsed);

  const [menu, setMenu] = useState(0)

  return (
    <>
    <div style={{
      width:"100vw",
      height:"70",
      textAlign:"center",
      backgroundColor:"orange",
      fontWeight:600,
      padding:5,
      fontSize:17
    }}>
      Book tours, attractions and things to do with GHUMORE.
    </div>
    <div className="bg-secondary" style={{display:"flex", flexDirection:"row"}}>
        <Link href="/">
          {/* <Image src={Logo} alt="Logo" width="36" height="36" className="vertical-align-middle" /> */}
          <a className="" style={{}}>
            <h3 className="" style={{marginLeft:10, marginTop:10}}>
            <Image src={logo} height={100} width={100} />
            </h3>
          </a>
        </Link>
    <nav className="navbar navbar-expand-sm navbar-light bg-secondary" style={{margin:"0 auto"}}>
      <div className="container">
        
        <button
          className="custom-toggler navbar-toggler"
          type="button" data-toggle="collapse"
          data-target="#navbarsExample09"
          aria-controls="navbarsExample09"
          aria-expanded={!isNavCollapsed ? true : false}
          aria-label="Toggle navigation"
          onClick={handleNavCollapse}
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className={`${isNavCollapsed ? 'collapse' : ''} navbar-collapse`}
          id="navbarsExample09"
        >
          <div className="navbar-nav">
            {links.map((value, index) => (
              <Link key={index} href={value.link} >
                <a className="nav-link"
                style={{
                  borderBottom: `${menu==index ? '1px solid red' : ''}`,
                  fontWeight: `${menu==index ? '700' : ''}`,
                  marginRight: 30
                }}
                onClick={()=>setMenu(index)}
                >{value.title}</a>
              </Link>
            ))}
            <Link  href="/signup">
										{/* <a className="btn btn-outline-primary my-1 mx-3" style={{width:120}}>
                    <FontAwesomeIcon icon={faUserPlus} style={{marginRight:15}} />
                      Sign Up
                    </a> */}
                    <button style={{
                      border: "2px solid black",
                      backgroundColor: "white",
                      borderColor: "#2152B1",
                      color: "#2152B1",
                      padding: "2px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5
                    }}>
                       <FontAwesomeIcon icon={faUserPlus} style={{marginRight:15}} />
                      Sign up
                      </button>
									</Link>
                  
                  <Link  href="/signin">
                  <button style={{
                      border: "2px solid black",
                      backgroundColor: "white",
                      backgroundColor: "#2152B1",
                      color: "#fff",
                      padding: "2px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      marginLeft:20
                    }}>
                       <FontAwesomeIcon icon={faRightToBracket} style={{marginRight:15}} />
                      Login
                      </button>
									</Link>
                  <Link  href="">
                  <button style={{
                      border: "2px solid black",
                      backgroundColor: "white",
                      borderColor: "#2152B1",
                      color: "#2152B1",
                      padding: "2px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      marginLeft:20
                    }}>
                       <FontAwesomeIcon icon={faCartShopping} style={{marginRight:15}} />
                      Cart
                      </button>
									</Link>
          </div>
        </div>
      </div>
    </nav>

    <div>
   
    </div>

 

    </div>

    <nav className="navbar navbar-expand-sm navbar-light bg-secondary" style={{margin:"0 auto", marginTop:30,
    display:`${menu==1 ? 'block' : 'none'}`
  }}>
      <div className="container">
        
        <button
          className="custom-toggler navbar-toggler"
          type="button" data-toggle="collapse"
          data-target="#navbarsExample09"
          aria-controls="navbarsExample09"
          aria-expanded={!isNavCollapsed ? true : false}
          aria-label="Toggle navigation"
          onClick={handleNavCollapse}
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className={`navbar-collapse`}
          id="navbarsExample09"
        >


          <div style={{display:"flex", flexDirection:"row"}}>
          <div className="navbar-nav" style={{display:"block"}}>

          <Link href={'#'} >
                <a className="nav-link"
                style={{
                  borderBottom: `1px solid red`,
                  fontWeight: `700`,
                  marginRight: 30,
                  marginTop:-15
                }}
                // onClick={()=>setMenu(index)}
                >All Experiences
                <FontAwesomeIcon icon={faCircleXmark} style={{marginLeft:10}} onClick={()=>setMenu(0)}/>
                </a>
              </Link>
              

            {links.map((value, index) => (
              // <Link key={index} href={value.link} >
                <a className="nav-link"
                style={{
                  // borderBottom: `${menu==index ? '1px solid red' : ''}`,
                  // fontWeight: `${menu==index ? '700' : ''}`,
                  marginRight: 30
                }}
                // onClick={()=>setMenu(index)}
                >{value.title}</a>
              // </Link>
            ))}
         
          </div>

          <div className="navbar-nav" style={{display:"block", marginTop:27}}>
            {links.map((value, index) => (
              // <Link key={index} href={value.link} >
                <a className="nav-link"
                style={{
                  // borderBottom: `${menu==index ? '1px solid red' : ''}`,
                  // fontWeight: `${menu==index ? '700' : ''}`,
                  marginRight: 30
                }}
                onClick={()=>setMenu(index)}
                >{value.title}</a>
              // </Link>
            ))}
         
          </div>

          <div className="navbar-nav" style={{display:"block", marginTop:27}}>
            {links.map((value, index) => (
              // <Link key={index} href={value.link} >
                <a className="nav-link"
                style={{
                  // borderBottom: `${menu==index ? '1px solid red' : ''}`,
                  // fontWeight: `${menu==index ? '700' : ''}`,
                  marginRight: 30
                }}
                onClick={()=>setMenu(index)}
                >{value.title}</a>
              // </Link>
            ))}
         
          </div>

          <div className="navbar-nav" style={{display:"block", marginTop:27}}>
            {links.map((value, index) => (
              // <Link key={index} href={value.link} >
                <a className="nav-link"
                style={{
                  // borderBottom: `${menu==index ? '1px solid red' : ''}`,
                  // fontWeight: `${menu==index ? '700' : ''}`,
                  marginRight: 30
                }}
                onClick={()=>setMenu(index)}
                >{value.title}</a>
              // </Link>
            ))}
         
          </div>

          </div>
          
        </div>
      </div>
    </nav>
    
    </>
  );
}
