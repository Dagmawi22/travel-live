import React from 'react';
import getConfig from 'next/config'
import Link from 'next/link'
import { useEffect, useState } from 'react';
import bg from "../public/imgs/bg.jpg"
import bg2 from "../public/imgs/bg2.jpg"
import bg3 from "../public/imgs/bg3.jpg"
import Image from 'next/image';
import { faPersonWalking, faLocationDot, faCircleDot } from '@fortawesome/free-solid-svg-icons';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const { publicRuntimeConfig } = getConfig()

export const Banner = ({ title, description, image, buttons }) => {

	const [img, setImg] = useState(1)

	const [windowWidth, setWindowWidth] = useState(0)

	useEffect(() => {
		setWindowWidth(window.innerWidth)
		
	  });

	return (
		<div className="" style={{width:"100vw"}}>
			<div className="">
				<div className="row align-items-center">
					
					<div className="col-sm-12 text-center">
						
							<Image
							className="my-3" width={windowWidth}
							height="550" src={bg}
							alt="profile of travel"
						/>
						
					</div>

					
					<div className="col-sm-12 text-center" style={{marginTop:"-600px", zIndex:100}}>
						<h3 className="" style={{
							color:"white",
							letterSpacing:1.6,
							fontSize:35,
							fontWeight:700,
						}}>
                            Bali Experience
                        </h3>

                        <div 
                        className="d-flex justify-content-center mt-4"
                       >
                            <button style={{
                      border: "2px solid #fa8a00",
                      backgroundColor: "#fa8a00",
                      color: "#fff",
                      padding: "10px 38px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      marginLeft:20
                    }}>
                       {/* <FontAwesomeIcon icon={faRightToBracket} style={{marginRight:15}} /> */}
                      Book Now
                      </button>

                      <button style={{
                      border: "2px solid #fff",
                      backgroundColor: "#fff",
                      color: "#2a2d32",
                      padding: "2px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      marginLeft:20
                    }}>
                       {/* <FontAwesomeIcon icon={faRightToBracket} style={{marginRight:15}} /> */}
                      Show Details
                      </button>

                        </div>

						
						
						</div>
					

				</div>
			</div>
		</div>
	);
}

export const About = ({ title, description }) => {
	return (
		<div id="about" className="bg-white py-5 px-5">
			<div className="container">
				<h1 className="text-primary fw-bold">{title}</h1>
				<div className="px-sm-5">
					{description.map((value, index) => (
						<p key={index} >{value}</p>
					))}
				</div>
			</div>
		</div>
	);
}