import React, { Profiler } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Image from "next/image";
import img from "../public/imgs/bg.jpg"
import Skydiving from "../public/Services/Skydiving.jpg"

export const Services = ({ title, cards, icons }) => {
  return (
    <div id="skills" className="py-5 px-5">
      <div className="container">
        <h1 className="text-center" style={{color: "#212529"}}>{title}</h1>
        <div className="d-flex flex-row flex-wrap justify-content-center" style={{padding:0}}>
          {cards.map((value, index) => (
            <Card
              key={index}
              title={value.title}
              description={value.description}
              link={value.link}
              icons={value.icons}
            />
          ))}
        </div>
      </div>
    </div>
  );
};


export const Card = ({ title, description }) => {
  return (
    <div
      className="card py-3 px-3 mx-sm-4 my-4 card-work"
      style={{ width: "20rem", height:"300px" }}
    >
      <Image src={Skydiving} layout="fill" />
      <h4 className="text-white text-center" style={{marginTop:200, marginLeft:-20, zIndex:"200", fontStyle:"Outfit", letterSpacing:1.5, fontSize:32}}>{title}</h4>
      {/* <h4 className="text-primary">{title}</h4> */}
      <div className="text-dark text-center">{description}</div>
      {/* <div className="text-end">
        <div>&rarr;</div>
      </div> */}
    </div>
  );
};

export const Card1 = ({ title, description }) => {
  return (
    <div
      className="card mx-sm-4 my-4 card-work"
      style={{ width: "20rem", borderRadius:5 }}
    >
      <Image src={Skydiving} width={200} height={200} />
      <h4 className="text-center" style={{color: "#112B3C", fontStyle:"Lato"}}>{title}</h4>
      {/* <h4 className="text-primary">{title}</h4> */}
      {/* <div className="text-dark text-center">{description}</div> */}
      {/* <div className="text-end">
        <div>&rarr;</div>
      </div> */}
    </div>
  );
};
