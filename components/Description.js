import React, { Fragment } from 'react';
import getConfig from 'next/config'
import Link from 'next/link';
import { Footer } from './Footer';
import { links, SEO, } from '../config/config';
import { Header } from './Header';
import Image from 'next/image';
import bg from "../public/imgs/bg.jpg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const { publicRuntimeConfig } = getConfig()

export default function Description() {
  return (
    <Fragment>
       
      <Header seo={SEO} />
      <div className="d-flex flex-column justify-content-between min-vh-100 py-2">
        <div className="py-5 px-5 container text-center">
        		<div className="row align-items-center py-5">
                {/* <div className="col-sm-4 text-center" style={{}}>
                  
                </div> */}

                <div className="text-center py-5" style={{marginLeft:"10vw", width:"80vw", backgroundColor:"#100534", minHeight:500,}}>
              
              <div style={{display:"flex", flexDirection:"row"}}>
                <div style={{marginTop:70, marginLeft:-150}}>
                    <Image src={bg} width={700} height={500} /> 
                </div>
             <div>
             <h3 className="" style={{marginTop:"20%", fontSize:44, color:"#fff"}}>Our Company</h3>
        
        <p style={{fontSize:24, color:"#fff", fontStyle:"Lato", textAlign:"justify"}} className='px-5 py-2'>
        GhumoRe India is an adventure tourism company founded in 20YY. Our vision is to be the most recognised and respected adventure business in India and in all over the world.
        </p>

        <button style={{
                      border: "2px solid black",
                      backgroundColor: "white",
                      backgroundColor: "#2152B1",
                      color: "#fff",
                      padding: "10px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      justifyContent:"left"
                    }}>
                      About Us
                      </button>

             </div>
                </div>


               
          
                </div>

</div>

    
        </div>
      
      </div>
    </Fragment>
  );
}

