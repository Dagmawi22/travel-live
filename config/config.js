import profile from "../public/imgs/bg.jpg";
import {
  faAppStore,
  faGithub,
  faGooglePlay,
  faCreativeCommonsSamplingPlus,
} from "@fortawesome/free-brands-svg-icons";
import {} from "@fortawesome/free-solid-svg-icons";

export const navigation = {
  name: "Dagmawi",
  links: [
    {
      title: "Home",
      link: "#about",
    },
    {
      title: "Experiences",
      link: "#",
    },
    {
      title: "Destinations",
      link: "#destinations",
    },
    {
      title: "Testimonies",
      link: "#testimonials",
    },
    {
      title: "About Us",
      link: "/links",
    },
    {
      title: "Contact Us",
      link: "/links",
    },
  ],
};

export const intro = {
  title: "Explore Incredible Destinations with us",
  description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.",
  image: profile.src,
  buttons: [
    {
      title: "Sign Up",
      link: "#contact",
      isPrimary: true,
    },
    {
      title: "Sign In",
      link: "https://drive.google.com/file/d/10Uw9ntso4s5aqYxdwEWf8JhGp7jBDRMF/view?usp=share_link",
      isPrimary: false,
    },
  ],
};

export const about = {
  title: "Who I am",
  description: [
    "Software developer specializing in web and mobile app development.",
    "Well-versed in numerous programming languages, frameworks and standards.",
    "Loves working in the descipline starting from software architecture and to final product for end users.",
  ],
};

export const work = {
  title: "Best Activities of the year",
  cards: [
    {
      title: "Cycling through Sirilanka",
      description:
        "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text",
      icons: null,
      img: profile.src
    },
    {
      title: "Doongi Dives",
      description: "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text",
      icons: null,
      img: profile.src
    },
    {
      title: "Shimla Sightseeing Tour",
      description:
        "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text",
      icons: null,
      img: profile.src
    },
    {
      title: "Cycling through Sirilanka",
      description:
        "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text",
      icons: null,
      img: profile.src
    },
    {
      title: "Shimla Sightseeing Tour",
      description:
        "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text",
      icons: null,
      img: profile.src
    },
    {
      title: "Doongi Dives",
      description: "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text Lorem ipsum is simply dummy text",
      icons: null,
      img: profile.src
    },
  ],
};

export const projects = {
  title: "Destinations",
  cards: [
    {
      title: "Hyderbaad",
      description:
        "",
      icons: [
        {
          icon: faCreativeCommonsSamplingPlus,
          link: "http://yetalle.42web.io",
        },
      ],
    },
    {
      title: "Mumbai",
      description:
        "",
      icons: [
        {
          icon: faAppStore,
          link: "https://mamelkecha.vercel.app",
        },
      ],
    },
    {
      title: "Port Blair",
      description:
        "",
      icons: [
        {
          icon: faAppStore,
          link: "https://mamelkecha.vercel.app",
        },
      ],
    },
    {
      title: "Chennai",
      description:
        "",
      icons: [
        {
          icon: faGithub,
          link: "https://app1.zebra-app.dev",
        },
      ],
    },

    {
      title: "Kolkata",
      description:
        "",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },

    {
      title: "Agra",
      description:
        "",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },
  ],
};

export const services = {
  title: "We offer a wide range of Experiences across World",
  cards: [
    {
      title: "Skydiving",
      description:
        "",
      icons: [
        {
          icon: faCreativeCommonsSamplingPlus,
          link: "http://yetalle.42web.io",
        },
      ],
    },
    {
      title: "Trekking",
      description:
        "",
      icons: [
        {
          icon: faAppStore,
          link: "https://mamelkecha.vercel.app",
        },
      ],
    },
    {
      title: "Paragliding",
      description:
        "",
      icons: [
        {
          icon: faAppStore,
          link: "https://mamelkecha.vercel.app",
        },
      ],
    },
    {
      title: "Rainforest Tours",
      description:
        "",
      icons: [
        {
          icon: faGithub,
          link: "https://app1.zebra-app.dev",
        },
      ],
    },

    {
      title: "Multi-Day Experiences",
      description:
        "",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },

    {
      title: "Tree Ziplining ",
      description:
        "",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },
  ],
};

export const exclusive = {
  title: "Promotion & Exclusive Offers",
  cards: [
    {
      title: "Rainforest Tours",
      description: "Lorem ipsum",
      icons: [
        {
          icon: faGithub,
          link: "https://app1.zebra-app.dev",
        },
      ],
    },

    {
      title: "Multi-Day Experiences",
      description: "Lorem ipsum",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },

    {
      title: "Tree Ziplining ",
      description: "Lorem ipsum",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },
  ],
};

export const testimonials = {
  cards: [
    {
      title: "UI/UX Designer",
      name: "Eleanor Pena",
      photo: "",
      description: "Customer testimonials are more effective than paid marketing copy as they take the spotlight away from the seller to shine it on the customers. ",
    },

    {
      title: "Vlogger",
      name: "Theresa Webb",
      photo: "",
      description: "In promotion and advertising, a testimonial or show consists of a person's written statement extolling the virtue of a product.",
    },

    {
      title: "Doctor",
      name: "Annette Black",
      photo: "",
      description: "Testimonials work because they aren''t strong sales pitches, they come across in an unbiased voice and establish trust",
    },
  ],
};

export const contact = {
  title: "Get in touch",
  description:
    "Please feel free to reach out directly by email at someone@travel.com.",
  buttons: [
    {
      title: "Email Us",
      link: "mailto:someone@travel.com",
      isPrimary: true,
    },
    {
      title: "Call",
      link: "tel:+251918888225",
      isPrimary: false,
    },
  ],
};

// SEARCH ENGINE
export const SEO = {
  // 50 - 60 char
  title: "Travel | Booking | Tour",
  description:
    "Lorem ipsum is simply...",
  image: profile.src,
};

export const links = {
  image: profile.src,
  title: "Travel",
  description: "Tour | Travel | Booking",
  cards: [
    {
      title: "Facebook",
      link: "",
    },
    {
      title: "Instagram",
      link: "",
    },
  ],
};

// https://www.npmjs.com/package/@dropzone-ui/react#dropzone-ui-react-components-api
