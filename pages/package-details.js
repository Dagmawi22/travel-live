import React, { Fragment, useEffect, useState } from 'react';
import getConfig from 'next/config'
import Link from 'next/link';
import { Footer } from '../components/Footer';
import { links, SEO, } from '../config/config';
import { Header } from '../components/Header';
import Image from 'next/image';
import bg from "../public/imgs/bg.jpg"
import logo from "../public/logo.svg"
import { faCar, faClock, faLanguage, faLocationDot, faMobile, faStar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const { publicRuntimeConfig } = getConfig()

export default function Home() {

    const [windowWidth, setWindowWidth] = useState(0)
    const [imgWidth, setImgWidth] = useState(0)

    useEffect(() => {
		setWindowWidth(window.innerWidth)
        setImgWidth((58.33/100)*innerWidth)
	});


  return (
    <Fragment>
         <Link href="/" style={{}}>
          {/* <Image src={Logo} alt="Logo" width="36" height="36" className="vertical-align-middle" /> */}
          <a className="" style={{backgroundColor:"red"}}>
            <h3 className="" style={{marginLeft:10, marginTop:10}}>
                <Image src={logo} height={100} width={100} />
            </h3>
          </a>
        </Link>
      <Header seo={SEO} />
      <div className="d-flex flex-column justify-content-between min-vh-100 py-1">
        <div className="py-2 px-5 container-fluid">
        		<div className="row align-items-center">
                
                <div className="col-sm-12">
                    <div>Home/ India Tours/ Water Tours</div>
                    <div style={{fontSize:40, fontWeight:600}}>Four-Day Private Luxury Golden Triangle Tour to Agra and Jaipur From New Delhi</div>

                    <div class="d-flex justify-content-start mx-4">
                    <div class="px-2 py-2" style={{color: "#ffbd39"}}>
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <span style={{color:"black", marginLeft:10}}>566</span>
                        </div>

                        <div class="px-2 py-2" style={{}}>
                        <FontAwesomeIcon icon={faLocationDot} style={{color:"#ed5826", marginRight:10}} />
                        <span style={{fontSize:17, fontWeight:600}}>New Delhi, India</span>
                        </div>

                        <div class="px-2 py-2" style={{}}>
                        <FontAwesomeIcon icon={faLocationDot} style={{color:"#ed5826", marginRight:10}} />
                        <span style={{fontSize:17, fontWeight:600}}>By Ghumore Tours</span>
                        </div>

                    </div>
                </div>
                
            <div className="py-2 container-fluid">
        		<div className="row py-1">

                <div className="col-sm-2 bg-secondary py-2">
                <div class="d-flex flex-column">
                <div class="p-2">
                    <Image src={bg} height={550} style={{borderRadius:5}} />
                    </div>
                    <div class="p-2">
                    <Image src={bg} height={550} style={{borderRadius:5}} />
                    </div>
                    <div class="p-2">
                    <Image src={bg} height={550} style={{borderRadius:5}} />
                    </div>
                    </div>
                </div>

                <div className="col-sm-7 py-2">

                <Image src={bg} width={imgWidth} height={650} style={{borderRadius:5}} />

                <div class="d-flex justify-content-start mx-4">
                    

                        <div class="px-2 py-2" style={{}}>
                        <FontAwesomeIcon icon={faClock} style={{color:"#ed5826", marginRight:10}} />
                        <span style={{fontSize:17, fontWeight:600}}>4 days</span>
                        </div>

                        <div class="px-2 py-2" style={{}}>
                        <FontAwesomeIcon icon={faCar} style={{color:"#ed5826", marginRight:10}} />
                        <span style={{fontSize:17, fontWeight:600}}>Hotel pickup offered</span>
                        </div>

                        <div class="px-2 py-2" style={{}}>
                        <FontAwesomeIcon icon={faMobile} style={{color:"#ed5826", marginRight:10}} />
                        <span style={{fontSize:17, fontWeight:600}}>Mobile ticket</span>
                        </div>

                        <div class="px-2 py-2" style={{}}>
                        <FontAwesomeIcon icon={faLanguage} style={{color:"#ed5826", marginRight:10}} />
                        <span style={{fontSize:17, fontWeight:600}}>
                        Offered in: English, Hindi, Russian, Spanish
                        </span>
                        </div>

                    </div>


                </div>

               
            </div>

            </div>

</div>

    
        </div>
        <Footer />
      </div>
    </Fragment>
  );
}


function Button({ title, link }) {
    return (
      <div className="row justify-content-center">
        <div className="mx-sm-4" style={{ width: "20rem", cursor:"pointer", marginTop:-25 }}>
          <Link href={link}>
              <h4 className="text-primary px-3">{title}</h4>
          </Link>
        </div>
      </div>
    );
  }
