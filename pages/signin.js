import React, { Fragment } from 'react';
import getConfig from 'next/config'
import Link from 'next/link';
import { Footer } from '../components/Footer';
import { links, SEO, } from '../config/config';
import { Header } from '../components/Header';
import Image from 'next/image';
import bg from "../public/imgs/bg.jpg"
import logo from "../public/logo.svg"

const { publicRuntimeConfig } = getConfig()

export default function Home() {
  return (
    <Fragment>
         <Link href="/" style={{}}>
          {/* <Image src={Logo} alt="Logo" width="36" height="36" className="vertical-align-middle" /> */}
          <a className="" style={{}}>
          <Image src={logo} height={100} width={100} style={{borderRadius:5}}/>
          </a>
        </Link>
      <Header seo={SEO} />
      <div className="d-flex flex-column justify-content-between min-vh-100 py-5">
        <div className="py-5 px-5 container text-center">
        		<div className="row align-items-center py-5">
                <div className="col-sm-6 text-center">
                   <Image src={bg} height={500} style={{borderRadius:5}}/>
                </div>

                <div className="col-sm-6 text-center">
                <h3 className="mt-3">SIGN IN</h3>
          
                <Image src={logo} height={100} width={100} style={{borderRadius:5}}/>
          
         
          <input type='email' placeholder="Email" className='form-control' style={{marginTop:20}}></input>
          
          
          <input type='password' placeholder="Password" className='form-control' style={{marginTop:20}}></input>
          
        
          
          <div className="row justify-content-center" style={{}}>
          <div className="card card-work mx-sm-4 mt-4" style={{ width: "20rem", cursor:"pointer", height:40, backgroundColor:"blue" }}> 
          <Link href="#">
              <h4 className="text-white py-1 px-3">Login</h4>
          </Link>
            </div>
          </div>

          <h4 className="py-1 px-3 py-4">Don&apos;t have an account?</h4>

            <Button  title="SIGN UP" link="/signup" />
          
                </div>

</div>

    
        </div>
        <Footer />
      </div>
    </Fragment>
  );
}


function Button({ title, link }) {
    return (
      <div className="row justify-content-center">
        <div className="mx-sm-4" style={{ width: "20rem", cursor:"pointer", marginTop:-25 }}>
          <Link href={link}>
              <h4 className="text-primary px-3">{title}</h4>
          </Link>
        </div>
      </div>
    );
  }
