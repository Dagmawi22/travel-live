import React, { Fragment } from 'react';
import getConfig from 'next/config'
import Link from 'next/link';
import { Footer } from '../components/Footer';
import { links, SEO, } from '../config/config';
import { Header } from '../components/Header';
import Image from 'next/image';
import bg from "../public/imgs/bg.jpg"
import logo from "../public/logo.svg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp, faDotCircle, faLocation, faLocationDot, faStar } from '@fortawesome/free-solid-svg-icons';

const { publicRuntimeConfig } = getConfig()

export default function Home() {
  return (
    <Fragment>
         <Link href="/" style={{}}>
          {/* <Image src={Logo} alt="Logo" width="36" height="36" className="vertical-align-middle" /> */}
          <a className="" style={{backgroundColor:"red"}}>
            <h3 className="" style={{marginLeft:10, marginTop:10}}>
                <Image src={logo} height={100} width={100} />
            </h3>
          </a>
        </Link>
      <Header seo={SEO} />
      <div className="d-flex flex-column justify-content-between min-vh-100">
        <div className="py-2 px-5 container-fluid">
        		<div className="row align-items-center py-2">
                
                <div className="col-sm-12">
                    <div style={{fontSize:40, fontWeight:600}}>India Tours, Activities & Excursions</div>

                    <span class="badge text-dark px-5 py-3" style={{marginRight:10, backgroundColor:"#f5f5f5", borderRadius:30}}>Cruises & Sailing</span>
                    <span class="badge text-dark px-5 py-3" style={{marginRight:10, backgroundColor:"#f5f5f5", borderRadius:30}}>Water Tours</span>
                    <span class="badge text-dark px-5 py-3" style={{marginRight:10, backgroundColor:"#f5f5f5", borderRadius:30}}>City Tours</span>

                    <div className="py-2 container-fluid">
        		<div className="row py-1">

                <div className="col-sm-2 bg-secondary py-2">
                <div class="d-flex flex-column">
                <div class="p-2">
                   <h5>All things to do</h5>
                    </div>
                    <div class="p-2">
                    <h6>Day Trips</h6>
                    <div class="d-flex flex-column mt-2">
                    <div class="p-1">Outdoor Activities</div>
                    <div class="p-1">Concerts & Shows</div>
                    <div class="p-1">Food & Drink</div>
                    <div class="p-1">Events</div>
                    <div class="p-1">Shopping</div>
                    <div class="p-1">Transportation</div>
                    <div class="p-1">Traveller Resources</div>
                    <div class="p-1" style={{color:"#2152b1", cursor:"pointer"}}>Show less
                    <FontAwesomeIcon icon={faChevronUp} style={{marginLeft:10}}/>
                    </div>
                    </div>
                    </div>
                    
                    <div class="p-2">
                    <h6>Locations</h6>
                    <div class="d-flex flex-column mt-2">
                    <div class="p-1">Hyderabad</div>
                    <div class="p-1">Delhi</div>
                    <div class="p-1">Mumbai</div>
                    <div class="p-1" style={{color:"#2152b1", cursor:"pointer"}}>Show more
                    <FontAwesomeIcon icon={faChevronDown} style={{marginLeft:10}}/>
                    </div>
                    </div>
                    </div>

                    <div class="p-2">
                    <h6>Duration</h6>
                    <div class="d-flex flex-column mt-2">
                    <div class="form-check p-1">
                    <input class="form-check-input" type="checkbox" id="check1" name="option1" value="something"  />
                    <label class="form-check-label">Up to 1 hour</label>
                    </div>

                    <div class="form-check p-1">
                    <input class="form-check-input" type="checkbox" id="check1" name="option1" value="something"  />
                    <label class="form-check-label">1 to 4 hours</label>
                    </div>

                    <div class="form-check p-1">
                    <input class="form-check-input" type="checkbox" id="check1" name="option1" value="something"  />
                    <label class="form-check-label">4 hours to 1 day</label>
                    </div>

                    <div class="form-check p-1">
                    <input class="form-check-input" type="checkbox" id="check1" name="option1" value="something"  />
                    <label class="form-check-label">1 to 3 days</label>
                    </div>

                    </div>
                    </div>

                    <div class="p-2">
                    <h6>Price</h6>
                    <div class="d-flex flex-column mt-2">
                        <div class="p-1">
                            <input type='range' />
                        </div>
                    </div>
                    </div>

                    </div>
                </div>

                <div className="col-sm-10 py-2" style={{}}>
                <div className="py-2 px-5 container-fluid">
        		<div className="row py-2">
                    <div className="col-sm-3 py-2 bg-secondary" style={{}}>
                        
                <Image src={bg} height={450} style={{borderRadius:5}} />
                    </div>

                    <div className="col-sm-9 py-4 bg-secondary" style={{}}>

                    <div class="d-flex justify-content-between">
                    <div style={{fontSize:24, fontWeight:700}}>
                        Four-Day Private Luxury Golden Triangle Tour to Agra and Jaipur From New Delhi
                        </div>

                        <button style={{
                      border: "2px solid #ed5826",
                      backgroundColor: "#ed5826",
                      color: "#fff",
                      padding: "2px 8px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:25,
                      marginLeft:20
                    }}>
                      4 days
                      </button>
                      </div>

                        
                        <div class="px-2 py-1" style={{color: "#ffbd39"}}>
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <span style={{color:"black", marginLeft:10}}>566</span>
                        </div>

                        <div class="px-2 py-1" style={{color: "#ed5826"}}>
                    <FontAwesomeIcon icon={faLocationDot} />
                    <span style={{color:"black", marginLeft:10}}>New Delhi, India</span>
                        </div>

                        <div class="px-2 py-1" style={{color: "#000000"}}>
                    <FontAwesomeIcon icon={faDotCircle} />
                    <span style={{color:"black", marginLeft:10}}>By Ghumore Tours</span>
                        </div>

                        <div class="px-2 py-1" style={{color: "#000000"}}>
                            <p>
                            Explore India's Golden Triangle on this four-day private tour traveling by air- conditioned vehicle with a local guide. In Delhi, visit Qutb Minar, Lotus Temple, India Gate, and Parliament House. Watch the sun rise...
                            </p>
                        </div>

                        <div className="px-2 py-1" style={{fontSize:14, fontWeight:400}}>
                        from
                        </div>

                        <div class="d-flex justify-content-between">
                        <div class="px-2" style={{fontSize:24, fontWeight:700}}>
                        ₹15,959.84
                        </div>

                        <button style={{
                      border: "2px solid #2152B1",
                      backgroundColor: "#2152B1",
                      color: "#fff",
                      padding: "2px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      marginLeft:20
                    }}>
                      Book Now
                      </button>
                      </div>


                    </div>

                </div>
                </div>

                <div className="py-2 px-5 container-fluid">
        		<div className="row py-2">
                    <div className="col-sm-3 py-2 bg-secondary" style={{}}>
                        
                <Image src={bg} height={450} style={{borderRadius:5}} />
                    </div>

                    <div className="col-sm-9 py-4 bg-secondary" style={{}}>

                    <div class="d-flex justify-content-between">
                    <div style={{fontSize:24, fontWeight:700}}>
                        Four-Day Private Luxury Golden Triangle Tour to Agra and Jaipur From New Delhi
                        </div>

                        <button style={{
                      border: "2px solid #ed5826",
                      backgroundColor: "#ed5826",
                      color: "#fff",
                      padding: "2px 8px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:25,
                      marginLeft:20
                    }}>
                      4 days
                      </button>
                      </div>

                        
                        <div class="px-2 py-1" style={{color: "#ffbd39"}}>
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <FontAwesomeIcon icon={faStar} />
                    <span style={{color:"black", marginLeft:10}}>566</span>
                        </div>

                        <div class="px-2 py-1" style={{color: "#ed5826"}}>
                    <FontAwesomeIcon icon={faLocationDot} />
                    <span style={{color:"black", marginLeft:10}}>New Delhi, India</span>
                        </div>

                        <div class="px-2 py-1" style={{color: "#000000"}}>
                    <FontAwesomeIcon icon={faDotCircle} />
                    <span style={{color:"black", marginLeft:10}}>By Ghumore Tours</span>
                        </div>

                        <div class="px-2 py-1" style={{color: "#000000"}}>
                            <p>
                            Explore India's Golden Triangle on this four-day private tour traveling by air- conditioned vehicle with a local guide. In Delhi, visit Qutb Minar, Lotus Temple, India Gate, and Parliament House. Watch the sun rise...
                            </p>
                        </div>

                        <div className="px-2 py-1" style={{fontSize:14, fontWeight:400}}>
                        from
                        </div>

                        <div class="d-flex justify-content-between">
                        <div class="px-2" style={{fontSize:24, fontWeight:700}}>
                        ₹15,959.84
                        </div>

                        <button style={{
                      border: "2px solid #2152B1",
                      backgroundColor: "#2152B1",
                      color: "#fff",
                      padding: "2px 28px",
                      fonSize: 19,
                      cursor: "pointer",
                      borderRadius:5,
                      marginLeft:20
                    }}>
                      Book Now
                      </button>
                      </div>


                    </div>

                </div>
                </div>



                </div>

               
            </div>

            </div>

                </div>

</div>

    
        </div>
        
      </div>

    

            <Footer />

    </Fragment>
  );
}


function Button({ title, link }) {
    return (
      <div className="row justify-content-center">
        <div className="mx-sm-4" style={{ width: "20rem", cursor:"pointer", marginTop:-25 }}>
          <Link href={link}>
              <h4 className="text-primary px-3">{title}</h4>
          </Link>
        </div>
      </div>
    );
  }
