import React, { Fragment } from 'react';
import { Nav } from '../components/Navbar';
import { Intro, About } from '../components/Intro';
import { Skills, Projects } from '../components/Work';
import { Footer, Contact } from '../components/Footer';
import Description from '../components/Description';
import { about, contact, exclusive, intro, navigation, projects, SEO, services, testimonials, work } from '../config/config';
import { Header } from '../components/Header';
import { Services } from '../components/Services';
import { Banner } from '../components/Banner';
import { Exclusive } from '../components/Exclusive';
import { Testimonal } from '../components/Testimonal';
import { Explore } from '../components/Explore';

export default function Home() {
  return (
    <Fragment>
      <Header seo={SEO} />
      <Nav
        title={navigation.name}
        links={navigation.links}
      />
      <Intro
        title={intro.title}
        description={intro.description}
        image={intro.image}
        buttons={intro.buttons}
      />
      {/* <About
        title={about.title}
        description={about.description}
      /> */}
      <Skills
        title={work.title}
        cards={work.cards}
      />
      <Projects
        title={projects.title}
        cards={projects.cards}
      />

      <Description />


    <Services
        title={services.title}
        cards={services.cards}
      />
      <Banner
        title={intro.title}
        description={intro.description}
        image={intro.image}
        buttons={intro.buttons}
      />

      <Exclusive
        title={exclusive.title}
        cards={exclusive.cards}
      />

      <Testimonal
       title={testimonials.title}
       cards={testimonials.cards}
       />

      {/* <Contact
        title={contact.title}
        description={contact.description}
        buttons={contact.buttons}
      /> */}
     <Explore
        title={intro.title}
        description={intro.description}
        image={intro.image}
        buttons={intro.buttons}
      />

      <Footer />
    </Fragment>
  );
}
